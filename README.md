# Qt6

This project builds Qt6 offline installers and caches them in the package
registry for faster installations during CI/CD jobs.
