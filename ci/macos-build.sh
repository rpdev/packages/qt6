#!/bin/bash

set -e

QT_INSTALLER="/tmp/qt-installer.dmg"
QT_URL="https://download.qt.io/official_releases/online_installers/qt-unified-mac-x64-online.dmg"

curl -L -o "$QT_INSTALLER" "$QT_URL"
mkdir -p /tmp/Volumes
hdiutil attach -mountpoint /tmp/Volumes "$QT_INSTALLER"

/tmp/Volumes/qt-online-installer-macOS-x64-*.app/Contents/MacOS/qt-online-installer-macOS-x64-* \
    install \
    \
    --na -m "$QT_LOGIN_USER" --pw "$QT_LOGIN_PASSWORD" \
    --accept-messages \
    --confirm-command \
    --accept-obligations \
    --accept-licenses \
    --auto-answer message.id=Ok \
    --no-force-installations \
    --no-default-installations \
    --root=$HOME/Qt \
    \
    qt.tools.cmake \
    qt.tools.ninja \
    qt.qt6.$QT_VERSION_INSTALLER.clang_64 \
    qt.qt6.$QT_VERSION_INSTALLER.ios \
    qt.qt6.$QT_VERSION_INSTALLER.addons.qtconnectivity \
    qt.qt6.$QT_VERSION_INSTALLER.addons.qtimageformats \
    qt.qt6.$QT_VERSION_INSTALLER.addons.qtlocation \
    qt.qt6.$QT_VERSION_INSTALLER.addons.qtnetworkauth \
    qt.qt6.$QT_VERSION_INSTALLER.addons.qtpositioning \
    qt.qt6.$QT_VERSION_INSTALLER.addons.qtremoteobjects \
    qt.qt6.$QT_VERSION_INSTALLER.addons.qtscxml

    # Other available addons:
    # qt.qt6.$QT_VERSION_INSTALLER.addons.qtwebengine \
    # qt.qt6.$QT_VERSION_INSTALLER.addons.qt3d \
    # qt.qt6.$QT_VERSION_INSTALLER.addons.qtdatavis3d \
    # qt.qt6.$QT_VERSION_INSTALLER.addons.qtwebview
    # qt.qt6.$QT_VERSION_INSTALLER.addons.qtserialbus \
    # qt.qt6.$QT_VERSION_INSTALLER.addons.qtserialport \
    # qt.qt6.$QT_VERSION_INSTALLER.addons.qtspeech \
    # qt.qt6.$QT_VERSION_INSTALLER.addons.qtvirtualkeyboard \
    # qt.qt6.$QT_VERSION_INSTALLER.addons.qtwebchannel \
    # qt.qt6.$QT_VERSION_INSTALLER.addons.qtwebsockets \
    # qt.qt6.$QT_VERSION_INSTALLER.addons.qtquickeffectmaker \
    # qt.qt6.$QT_VERSION_INSTALLER.addons.qtpdf \
    # qt.qt6.$QT_VERSION_INSTALLER.addons.qtquick3dphysics \
    # qt.qt6.$QT_VERSION_INSTALLER.addons.qthttpserver \
    # qt.qt6.$QT_VERSION_INSTALLER.addons.qtlottie \
    # qt.qt6.$QT_VERSION_INSTALLER.addons.qtgrpc \
    # qt.qt6.$QT_VERSION_INSTALLER.addons.qtcharts \
    # qt.qt6.$QT_VERSION_INSTALLER.addons.qtgraphs \
    # qt.qt6.$QT_VERSION_INSTALLER.addons.qtinsighttracker \
    # qt.qt6.$QT_VERSION_INSTALLER.addons.qtlanguageserver \
    # qt.qt6.$QT_VERSION_INSTALLER.addons.qtmultimedia \
    # qt.qt6.$QT_VERSION_INSTALLER.addons.qtsensors \

ARCHIVE_FILE_MACOS=$PWD/Qt-MacOS.zip
ARCHIVE_FILE_IOS=$PWD/Qt-iOS.zip
ARCHIVE_FILE_TOOLS=$PWD/Qt-Tools.zip

# Clean up to reduce package size:
pushd $HOME/Qt/$QT_VERSION
## Remove contents of debug symbol files:
find . -name "*.dSYM" -type d | rm -rf
## Truncate debug archives (we cannot remove them as then CMake might complain):
find . -name "*_debug.a" -exec truncate -s 0 "{}" \;
popd

pushd $HOME
zip -m -qr9 "$ARCHIVE_FILE_MACOS" Qt/$QT_VERSION/macos
zip -m -qr9 "$ARCHIVE_FILE_IOS" Qt/$QT_VERSION/ios
zip -m -qr9 "$ARCHIVE_FILE_TOOLS" Qt/Tools
popd

curl \
    --header "JOB-TOKEN: $CI_JOB_TOKEN" \
    --upload-file $ARCHIVE_FILE_MACOS \
    "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/Qt6/$QT_VERSION/Qt-MacOS.zip"
curl \
    --header "JOB-TOKEN: $CI_JOB_TOKEN" \
    --upload-file $ARCHIVE_FILE_IOS \
    "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/Qt6/$QT_VERSION/Qt-iOS.zip"
curl \
    --header "JOB-TOKEN: $CI_JOB_TOKEN" \
    --upload-file $ARCHIVE_FILE_TOOLS \
    "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/Qt6/$QT_VERSION/Qt-Tools.zip"
