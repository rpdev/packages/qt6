$ErrorActionPreference = "Stop"

# Configuration:
$QT_INSTALLER_URL="https://download.qt.io/official_releases/online_installers/qt-unified-windows-x64-online.exe"

# Install Qt
Invoke-WebRequest -o "qt-unified.exe" $QT_INSTALLER_URL

if (-not $?) {
    Write-Error -Message "Failed to download Qt installer."
}

.\qt-unified.exe `
    install `
    `
    --na -m "$env:QT_LOGIN_USER" --pw "$env:QT_LOGIN_PASSWORD" `
    --accept-messages `
    --confirm-command `
    --accept-obligations `
    --accept-licenses `
    --auto-answer message.id=Ok `
    --no-force-installations `
    --no-default-installations `
    --root=C:\Qt `
    `
    qt.tools.cmake `
    qt.tools.ninja `
    qt.qt6.$env:QT_VERSION_INSTALLER.win64_llvm_mingw `
    qt.qt6.$env:QT_VERSION_INSTALLER.addons.qtconnectivity `
    qt.qt6.$env:QT_VERSION_INSTALLER.addons.qtimageformats `
    qt.qt6.$env:QT_VERSION_INSTALLER.addons.qtlocation `
    qt.qt6.$env:QT_VERSION_INSTALLER.addons.qtnetworkauth `
    qt.qt6.$env:QT_VERSION_INSTALLER.addons.qtpositioning `
    qt.qt6.$env:QT_VERSION_INSTALLER.addons.qtremoteobjects `
    qt.qt6.$env:QT_VERSION_INSTALLER.addons.qtscxml

if (-not $?) {
    Write-Error -Message "Failed to install Qt."
}

$compress = @{
    Path = "C:\Qt\$env:QT_VERSION", "C:\Qt\Tools"
    DestinationPath = "Qt-llvm-mingw-w64.zip"
  }
Compress-Archive @compress

$Uri = "$env:CI_API_V4_URL/projects/$env:CI_PROJECT_ID/packages/generic/Qt6/$env:QT_VERSION/Qt-llvm-mingw-w64.zip"
$Headers = @{
    'JOB-TOKEN' = "$env:CI_JOB_TOKEN"
}
Invoke-WebRequest `
    -Uri $Uri `
    -UseBasicParsing `
    -Headers $Headers `
    -Method put `
    -InFile Qt-llvm-mingw-w64.zip `
    -ContentType "application/octet-stream"
